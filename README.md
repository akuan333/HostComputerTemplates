# 单机应用快速开发模板

#### 介绍
上位机开发模板，集成用户权限，设备添加等  
本人要服兵役了，没精力搞了，原本还想完善了在开源的，但现在实在没精力搞了。  
QQ群：951759865

#### 软件架构
软件架构说明  
1：使用反射实现的小型IOC  
2：单例交付IOC管理  
3：用户权限  
4：设备管理  
5：系统管理  

#### 安装教程

1.  发布只能选单应用发布,不要选ReadyToRun,这个不会编译第三方DLL某些方法。  

#### 使用说明

1.  多看【\Tools\Helpers\】里面的扩展类，大多数都封装了一遍。像跨线程，写日志，界面的报警滚动条等等。  
2.	本框架使用自己写的IOC管理单例对象，对象标记特性[Attributes.Singleton]即为单例，获取对象为 var model = Domains.ObjectContainer.ResolveSingleton<T>();。  
3.	自己添加界面需要在页面下的添加ViewFeature特性，详细请看例子和注释。  
4.	Servers目录下的TaskService类封装了循环任务，编写一个类继承IMainTask接口即可，有参数的Handle为循环任务，无参数的Handle为循环任务结束后执行的操作。  
5.	设备通信的接口没有完全封装（没精力写了，准备当兵），需要自己继承ICommunication写一下。  

#### 特技

1：UI框架采用开源的：MaterialDesign  链接：https://github.com/MaterialDesignInXAML/MaterialDesignInXamlToolkit  
2：滚动条代码是从HandyControl扒出来的 链接：https://github.com/HandyOrg/HandyControl  
3：使用反射实现IOC应用，但是并不完善，属于够用阶段，界面GC还没写，所有最好还是使用单例的界面。   