﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using UIWindows.Domains;
using UIWindows.Tools.Helpers;
using UIWindows.GlobalStatics;

namespace UIWindows
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string device = AppDomain.CurrentDomain.BaseDirectory.Substring(0, 1);
            int count = Process.GetProcessesByName(System.Reflection.Assembly.GetEntryAssembly().FullName.Split(',')[0]).Length;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
            if (!Directory.Exists(GlobalStatic.ConfigPath)) { _ = Directory.CreateDirectory(GlobalStatic.ConfigPath); }
            if ((count > 1) || (GetHardDiskFreeSpace(AppDomain.CurrentDomain.BaseDirectory.Substring(0, 1)) < 5))
            {
                _ = MessageBox.Show(count > 1 ? $"不可以重复打开软件！！！" : $"{device}硬盘容量过小，请手动清理垃圾或删除旧数据", "Xing丶Lucifer");
                Current.Shutdown();
            }
            _ = log4net.Config.XmlConfigurator.Configure(new FileInfo(GlobalStatic.Log4NetPath));
            _ = ViewModelLocator.Instance;
            DialogLogHelper.Init();
        }
        private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            e.ToString().RunLog(Enums.MessageLevelType.严重);
        }

        private static long GetHardDiskFreeSpace(string str_HardDiskName)
        {
            DriveInfo drive = DriveInfo.GetDrives().Where(x => x.Name == $"{str_HardDiskName}:\\").FirstOrDefault();
            return drive != null ? drive.TotalFreeSpace / (1024 * 1024 * 1024) : 0;
        }
    }
}
