﻿using MaterialDesignThemes.Wpf;
using System;
using UIWindows.Enums;

namespace UIWindows.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class ViewFeatureAttribute : Attribute
    {
        /// <summary>
        /// 菜单名字
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 是否是单例
        /// </summary>
        public bool IsSingleton { get; set; }
        /// <summary>
        /// 视图排序
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 显示权限
        /// </summary>
        public AuthorityType Level { get; set; }
        /// <summary>
        /// 视图图标
        /// </summary>
        public PackIconKind MenuIcon { get; set; }
        public ViewFeatureAttribute()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuname">显示名称</param>
        /// <param name="index">排序用</param>
        public ViewFeatureAttribute(string menuname, int index) : this()
        {
            MenuName = menuname;
            Index = index;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuname">显示名称</param>
        /// <param name="index">排序用</param>
        /// <param name="isSingleton">是否单例</param>
        public ViewFeatureAttribute(string menuname, int index, bool isSingleton) : this(menuname, index)
        {
            IsSingleton = isSingleton;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuname">显示名称</param>
        /// <param name="index">排序用</param>
        /// <param name="isSingleton">是否单例</param>
        /// <param name="userLevel">可显示等级</param>
        public ViewFeatureAttribute(string menuname, int index, bool isSingleton, AuthorityType userLevel) : this(menuname, index, isSingleton)
        {
            Level = userLevel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuname">显示名称</param>
        /// <param name="index">排序用</param>
        /// <param name="isSingleton">是否单例</param>
        /// <param name="userLevel">可显示等级</param>
        /// <param name="iconKind">显示图标</param>
        public ViewFeatureAttribute(string menuname, int index, bool isSingleton, AuthorityType userLevel, PackIconKind iconKind) : this(menuname, index, isSingleton, userLevel)
        {
            MenuIcon = iconKind;
        }
    }
}
