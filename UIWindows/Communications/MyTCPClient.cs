﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIWindows.Enums;
using UIWindows.XingLucifer;

namespace UIWindows.Communications
{
    public class MyTCPClient : ICommunication
    {
        private bool _isOnline;
        /// <summary>
        /// 是否在线
        /// </summary>
        public bool IsOnline { get => _isOnline; }

        private int _index;
        public int Index { get => _index; }

        private int _port;
        public int Port { get => _port; }

        private string _ipcom;
        public string IPCOM { get => _ipcom; }

        private DeviceTypes _types;
        public DeviceTypes Types { get => _types; }

        private List<byte> _cachebyte;
        public List<byte> CacheByte { get => _cachebyte; }
        public int Timeout { get; set; }
        private CancellationToken _socketToken;
        private TcpClient _client;
        private NetworkStream? _stream;
        private int _count;

        public MyTCPClient(CancellationToken cancellation)
        {
            _cachebyte = new List<byte>();
            _socketToken = cancellation;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="index">序号</param>
        /// <param name="port">端口号</param>
        /// <param name="ipcom">IP或COM号</param>
        /// <param name="types">设备类型</param>
        /// <param name="count">重连次数</param>
        public MyTCPClient(int index, int port, string ipcom, DeviceTypes types, CancellationToken cancellation, int count = 3)
            : this(cancellation)
        {
            _index = index;
            _port = port;
            _ipcom = ipcom;
            _types = types;
            _count = count;
        }

        public bool WriteSingle<TValue>(TValue value, int address, int offset, int add_type, int count = 3) where TValue : struct
        {
            if (count == 1) return false;
            Type type = typeof(TValue);
            if (type.IsArray)
            {
                throw new Exception("请使用Write方法写入数组数据");
            }
            switch (type.Name)
            {
                case "Boolean": break;
                case "Int32": break;
                case "UInt32": break;
                case "Int16": break;
                case "UInt16": break;
                case "Single": break;
                case "Double": break;
                case "Byte": break;
                case "Int64": break;
                case "UInt64": break;
            }
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return false;
                return WriteSingle(value, address, offset, add_type, --_count);
            }
            return true;
        }

        public bool Write<TArray>(TArray value, int address, int offset, int length, int add_type, int count = 3) where TArray : struct
        {
            if (count == 1) return false;
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return false;
                return Write(value, address, offset, length, add_type, --_count);
            }
            return true;
        }

        public TValue? ReadSingle<TValue>(int address, int offset, int add_type, int count = 3) where TValue : struct
        {
            if (count == 1) return null;
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return null;
                return ReadSingle<TValue>(address, offset, add_type, --_count);
            }
            return null;
        }

        public TArray? Read<TArray>(int address, int offset, int length, int add_type, int count = 3) where TArray : struct
        {
            if (count == 1) return null;
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return null;
                return Read<TArray>(address, offset, length, add_type, --_count);
            }
            return null;
        }

        public TClass? ReadClass<TClass>(int address, int offset, int add_type, int count = 3) where TClass : class
        {
            if (count == 1) return null;
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return null;
                return ReadClass<TClass>(address, offset, add_type, --_count);
            }
            return null;
        }

        public TStruct? ReadStruct<TStruct>(int address, int offset, int add_type, int count = 3) where TStruct : struct
        {
            if (count == 1) return null;
            try
            {

            }
            catch (Exception)
            {
                _isOnline = false;
                if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                else return null;
                return ReadStruct<TStruct>(address, offset, add_type, --_count);
            }
            return null;
        }

        public async Task Read()
        {
            await Task.Run(() =>
            {
                while (!_socketToken.IsCancellationRequested)
                {
                    try
                    {

                    }
                    catch (Exception)
                    {
                        _isOnline = false;
                        if (!_socketToken.IsCancellationRequested) ReconnectDisconnection?.Invoke(this, Index, IPCOM, Port, Types, _socketToken);
                    }
                    Thread.Sleep(50);
                }
            });
        }

        public bool Open()
        {
            _client = new TcpClient();
            _client.ReceiveTimeout = Timeout;
            _client.SendTimeout = Timeout;
            _client.ConnectAsync(IPCOM, Port).GetAwaiter().GetResult();
            _stream = _client.GetStream();
            try
            {

                _isOnline = true;
            }
            catch (Exception ex)
            {
                _isOnline = false;
                return false;
            }
            return true;
        }

        public void Close()
        {
            try
            {
                _client.Close();
                _client.Dispose();
                _stream.Dispose();
            }
            catch (Exception)
            {
                _isOnline = false;
            }
        }

        public Func<ICommunication, int, string, int, DeviceTypes, CancellationToken, bool> ReconnectDisconnection { get; set; }

        public bool Write(byte[] bytes, int length)
        {

            return false;
        }

        public byte[] Read(byte[] bytes, int length, CancellationTokenSource tokenSource)
        {
            return new byte[1024];
        }
    }
}
