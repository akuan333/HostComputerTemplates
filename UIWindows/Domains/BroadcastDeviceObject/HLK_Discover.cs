﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Models;
using UIWindows.XingLucifer;

namespace UIWindows.Domains.BroadcastDeviceObject
{
    public class HLK_Discover : ISerialServer
    {
        public int Port { get; } = 988;

        public byte[] GetStream() => Encoding.ASCII.GetBytes("HLK");

        public SerialServerModel Parsing(byte[] bytes, IPEndPoint endpoint)
        {
            string mark = Encoding.ASCII.GetString(bytes);
            if (!mark.Contains("HLK")) return new SerialServerModel() { Mark = "错误" };
            return new SerialServerModel()
            {
                Mark = mark,
                IPAddress = endpoint.Address.ToString(),
            };
        }
    }
}
