﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Models;
using UIWindows.XingLucifer;

namespace UIWindows.Domains.BroadcastDeviceObject
{
    public class IP_Search_1_2 : ISerialServer
    {
        public int Port { get; } = 910;

        public byte[] GetStream() => new byte[17] {
            0x01,0x11,0xE2,0x39,0x1C,0xE3,0x74,0x10,
            0x63,0xDE,0xEE,0x3A,0x2E,0xEF,0x0C,0x8D,
            0x9C
        };

        public SerialServerModel Parsing(byte[] bytes, IPEndPoint endpoint)
        {
            if (bytes.Length < 64) return new SerialServerModel() { Mark = "Error" };
            byte[] mac = bytes.Skip(41).Take(6).ToArray();
            return new SerialServerModel()
            {
                Mark = Encoding.ASCII.GetString(bytes.Take(9).ToArray()),
                MAC = $"{mac[0]:X2}:{mac[1]:X2}:{mac[2]:X2}:{mac[3]:X2}:{mac[4]:X2}:{mac[5]:X2}",
                IPAddress = string.Join(".", bytes.Skip(47).Take(4).ToArray()),
                Number = bytes[51]
            };
        }
    }
}
