﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UIWindows.XingLucifer;

namespace UIWindows.Domains
{
    public class TaskManager
    {
        private static ConcurrentDictionary<string, object> _taskcd = new ConcurrentDictionary<string, object>();

        static TaskManager()
        {
            _taskcd = new ConcurrentDictionary<string, object>();
        }

        public static void Publish<T>(T context) where T : IDataContext, new()
        {
            string key = $"{context.Types.GetHashCode()}{context.AddressSignal}{context.AddressComplete}";
            if (_taskcd.ContainsKey(key) || !_taskcd.TryAdd(key, context)) return;
            object obj = ObjectContainer.ResolveEvent(context.GetType());
            MethodInfo method = obj.GetType().GetMethod("Handle");
            _ = Task.Run(new Action(() => method?.Invoke(obj, new object[] { context }))).ConfigureAwait(false);
        }

        public static bool RemoveTask<T>(T context) where T : IDataContext, new() => _taskcd.TryRemove($"{context.Types.GetHashCode()}{context.AddressSignal}{context.AddressComplete}", out _);

        /// <summary>
        /// 移除所有任务
        /// </summary>
        public static void RemoveAll() => _taskcd.Clear();
    }
}
