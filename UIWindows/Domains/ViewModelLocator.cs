﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using UIWindows.GlobalStatics;
using UIWindows.Models;

namespace UIWindows.Domains
{
    public class ViewModelLocator : ViewModelBase
    {
        private object _viewContent;

        public object ViewContent
        {
            get { return _viewContent; }
            set => SetProperty(ref _viewContent, value);
        }

        public ObservableCollection<MenuModel> ContentList { get; set; }

        private MenuModel _selectedItem;

        public MenuModel SelectedItem
        {
            get { return _selectedItem; }
            set {
                if (value != null && _selectedItem != value)
                {
                    _selectedItem = value;
                    ViewContent = ObjectContainer.ResolvePage(value.Name);
                }
            }
        }

        private readonly static object _obj = new object();
        private static ViewModelLocator _modelLocator;
        public static ViewModelLocator Instance
        {
            get
            {
                if (_modelLocator == null)
                {
                    lock (_obj)
                    {
                        if (_modelLocator == null) _modelLocator = new ViewModelLocator();
                    }
                }
                return _modelLocator;
            }
        }

        private ViewModelLocator()
        {
            ContentList = new ObservableCollection<MenuModel>();
            _= ObjectContainer.Init;
        }

        public void UpdateMenu(Enums.AuthorityType authority)
        {
            ContentList.Clear();
            foreach (var item in ObjectContainer.GetMenuList(authority))
            {
                ContentList.Add(new MenuModel() { Name = item.Name, Icon = item.Icon });
            }
            if (ContentList.Count > 0) SelectedItem = ContentList[0];
        }
    }
}
