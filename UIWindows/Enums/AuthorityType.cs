﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Enums
{
    /// <summary>
    /// 权限枚举
    /// </summary>
    public enum AuthorityType : ushort
    {
        ALL = 0b1111_1111_1111_1111,
        普工 = 0b0000_0000_0000_0001,
        班长 = 0b0000_0000_0000_0010 | 普工,
        组长 = 0b0000_0000_0000_0100 | 班长,
        主管 = 0b0000_0000_0000_1000 | 组长,
        工程师 = 0b0000_0000_0001_0000 | 主管,
        管理员 = 0b0000_0000_0010_0000 | 工程师,


        班长_参照 = 0b0000_0000_0000_0010,
        组长_参照 = 0b0000_0000_0000_0100,
        主管_参照 = 0b0000_0000_0000_1000,
        工程师_参照 = 0b0000_0000_0000_1000,
        管理员_参照 = 0b0000_0000_0001_0000,
        无权限 = 0,
    }
}
