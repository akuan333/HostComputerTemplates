﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Enums
{
    public enum DeviceTypes : byte
    {
        固定扫码枪,
        手持扫码枪,
        AND电子秤,
        TC06电子秤,
        西门子S7,
        欧姆龙Fins,
        欧姆龙C_Mode,
        三菱3Q,
        ModbusTCP,
        ModbusRTU,
        ModbusASCII,
        OPC_UA,

    }
}
