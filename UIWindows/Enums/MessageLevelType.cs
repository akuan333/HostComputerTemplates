﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Enums
{
    /// <summary>
    /// 日志消息的级别
    /// </summary>
    public enum MessageLevelType : byte
    {
        紧急,
        警告,
        严重,
        错误,
        提醒,
        注意,
        信息,
        调试,
    }
}
