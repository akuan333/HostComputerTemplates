﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Enums
{
    /// <summary>
    /// 任务枚举
    /// </summary>
    public enum TaskTypes : byte
    {
        前扫码,
        后扫码,
        出站扫码,
    }
}
