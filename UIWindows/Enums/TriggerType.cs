﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Enums
{
    public enum TriggerType : byte
    {
        上升沿,
        下降沿,
        值变化,
    }
}
