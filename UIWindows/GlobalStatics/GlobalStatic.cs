﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.GlobalStatics
{
    /// <summary>
    /// 全局静态参数
    /// </summary>
    public static class GlobalStatic
    {
        private static string _path = Environment.CurrentDirectory;

        public static string Log4NetPath { get => $"{_path}\\log4net.config"; }
        public static string ConfigPath { get => $"{_path}\\config"; }
        public static string DevicePath { get => $"{ConfigPath}\\device.txt"; }
        public static string ParameterPath { get => $"{ConfigPath}\\parameter.txt"; }
        public static string SystemConfigPath { get => $"{ConfigPath}\\systemconfig.txt"; }
        public static string UserPath { get => $"{ConfigPath}\\user.txt"; }

        public static string DES_KEY { get => "12560695"; }
        public static string DES_IV { get => "k164s453"; }

        public static bool IsDeBug { get; } = false;

        public static int DBReadNumber { get => 200; }
        public static int DBWriteNumber { get => 200; }
    }
}
