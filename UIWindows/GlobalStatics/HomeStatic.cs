﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Models;

namespace UIWindows.GlobalStatics
{
    [Attributes.Singleton]
    public class HomeStatic
    {
        public UserControls.Tools.AsyncObservableCollection<RunMessageModel> LogList { get; set; }
        public HomeStatic()
        {
            LogList = new UserControls.Tools.AsyncObservableCollection<RunMessageModel>();
        }
    }
}
