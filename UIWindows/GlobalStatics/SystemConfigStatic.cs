﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using UIWindows.Models;
using System.Text.Json.Serialization;
using PropertyChanged;

namespace UIWindows.GlobalStatics
{
    [Attributes.Singleton]
    public class SystemConfigStatic
    {
        public ScrollBarModel MainMvvm { get; set; }
        public SystemConfigModel SCM { get; set; }

        public SystemConfigStatic()
        {
            if (!File.Exists(GlobalStatic.SystemConfigPath))
            {
                MainMvvm = new ScrollBarModel();
                SCM = new SystemConfigModel();
            }
            else
            {
                var model = JsonSerializer.Deserialize<Dictionary<string, object>>(File.ReadAllText(GlobalStatic.SystemConfigPath, Encoding.UTF8));
                MainMvvm = JsonSerializer.Deserialize<ScrollBarModel>(model["MainMvvm"].ToString());
                SCM = JsonSerializer.Deserialize<SystemConfigModel>(model["SCM"].ToString());
            }
        }

        public void Save() => File.WriteAllText(GlobalStatic.SystemConfigPath, JsonSerializer.Serialize(this), Encoding.UTF8);
    }
}
