﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using UIWindows.Domains;
using UIWindows.Enums;
using UIWindows.Models;
using UIWindows.Tools.Helpers;

namespace UIWindows.GlobalStatics
{
    [Attributes.Singleton]
    public class UserConfigStatic
    {
        public ObservableCollection<UserModel> UserList { get; set; }
        public UserModel User { get; set; }
        public UserModel LoginUser { get; set; }
        public AuthorityType LoginAuthority { get; set; } = AuthorityType.管理员;
        public UserConfigStatic()
        {
            User = new UserModel() { Level = AuthorityType.ALL };
            UserList = !File.Exists(GlobalStatic.UserPath)
                ? new ObservableCollection<UserModel>()
                : JsonSerializer.Deserialize<ObservableCollection<UserModel>>(DESHelper.DESDecrypt(File.ReadAllText(GlobalStatic.UserPath, Encoding.UTF8), GlobalStatic.DES_KEY, GlobalStatic.DES_IV));
        }

        public void Save()
        {
            UserModel model = UserList.FirstOrDefault(x => x.Name == User.Name);
            if (model == null && User.Name != "admin")
            {
                UserList.Add(new UserModel()
                {
                    Address = User.Address,
                    Department = User.Department,
                    Shift = User.Shift,
                    FrameTime = User.FrameTime,
                    IsFrameTime = User.IsFrameTime,
                    Level = User.Level,
                    Name = User.Name,
                    Password = User.Password,
                    PhoneNumber = User.PhoneNumber,
                    RegisterTime = DateTime.Now,
                });
            }
            else
            {
                User.UpdateTime = DateTime.Now;
                User.UpdateValue(model);
            }
            SaveText();
        }

        public void Delect()
        {
            UserModel model = UserList.FirstOrDefault(x => x.Name == User.Name && x.Name != "admin");
            if (model != null)
            {
                UserList.Remove(model);
                SaveText();
            }
        }

        private void SaveText() => File.WriteAllText(GlobalStatic.UserPath, DESHelper.DESEncrypt(JsonSerializer.Serialize(UserList), GlobalStatic.DES_KEY, GlobalStatic.DES_IV), Encoding.UTF8);

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="passworid">密码</param>
        /// <returns></returns>
        public bool Login(string name, string passworid)
        {
            var model = UserList.FirstOrDefault(x => x.Name == name && x.Password == passworid);
            if (model != null)
            {
                LoginAuthority = model.Level;
                model.LoginTime = DateTime.Now;
                LoginUser = model;
                ViewModelLocator.Instance.UpdateMenu(LoginAuthority);
                SaveText();
                return true;
            }
            if (name == "admin" && passworid == "admin22345")
            {
                LoginAuthority = AuthorityType.管理员;
                ViewModelLocator.Instance.UpdateMenu(LoginAuthority);
                LoginUser = new UserModel() { Name = "管理员", Level = AuthorityType.管理员 };
                return true;
            }
            return false;
        }
    }
}
