﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UIWindows.ViewModels;
using UIWindows.ViewModels.DialogModels;
using UIWindows.Views;
using UIWindows.Views.Dialogs;
using UIWindows.Tools.Helpers;
using UIWindows.GlobalStatics;
using System.Threading;
using UIWindows.Domains;

namespace UIWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DialogHost_Loaded(object sender, RoutedEventArgs e)
        {
            _ = DialogHost.Show(new LoginDialogs() { DataContext = new LoginDialogsViewModel() }, "RootDialog").ConfigureAwait(false);
            DataContext = new MainWindowViewModel();
            WindowState = WindowState.Maximized;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var model = ObjectContainer.ResolveSingleton<SystemConfigStatic>();
            if (model.MainMvvm.IsRunStatus || DialogHost.IsDialogOpen("RootDialog")) { e.Cancel = true; }
            else if (MessageBox.Show("确定是退出吗？", "询问", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) { e.Cancel = true; }            
        }
    }
}
