﻿using PropertyChanged;
using System.ComponentModel;
using System.Text.Json.Serialization;
using UIWindows.Tools.Helpers;
using UIWindows.XingLucifer;

namespace UIWindows.Models
{
    /// <summary>
    /// 设备集合
    /// </summary>
    public class DeviceClientModel : INotifyPropertyChanged
    {
        /// <summary>
        /// 通信接口
        /// </summary>
        [JsonIgnore]
        [DoNotNotify]
        public ICommunication Communication { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// 波特率或端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 是否网络设备
        /// </summary>
        [DoNotNotify]
        public bool IsSocket { get; set; }

        /// <summary>
        /// IP或端口号
        /// </summary>
        public string IPCOM { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        public Enums.DeviceTypes DeviceType { get; set; }

        /// <summary>
        /// ping时间
        /// </summary>
        public long PingTime { get; set; }

        /// <summary>
        /// Ping结果
        /// </summary>
        public ICMPType ICMPResult { get; set; }

        /// <summary>
        /// 是否在线
        /// </summary>
        [JsonIgnore]
        public bool? IsOnline { get; set; } = null;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
