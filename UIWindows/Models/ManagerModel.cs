﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Enums;

namespace UIWindows.Models
{
    public class ManagerModel
    {
        /// <summary>
        /// 任务名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        public TriggerType Types { get; set; }
        /// <summary>
        /// 任务事件
        /// </summary>
        public Action<object> TaskEvent { get; set; }
    }
}
