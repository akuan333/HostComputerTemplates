﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Models
{
    public class MenuModel
    {
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
    }
}
