﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Enums;

namespace UIWindows.Models
{
    public class MenuViewModel
    {
        /// <summary>
        /// 菜单名字
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 是否是单例
        /// </summary>
        public bool IsSingleton { get; set; }
        /// <summary>
        /// 视图排序
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 显示权限
        /// </summary>
        public AuthorityType Level { get; set; }
        /// <summary>
        /// 视图图标
        /// </summary>
        public PackIconKind MenuIcon { get; set; }
        /// <summary>
        /// 单例时为Null
        /// </summary>
        public Type View { get; set; } = null;
        /// <summary>
        /// 单例时有对象
        /// </summary>
        public object Context { get; set; } = null;
        /// <summary>
        /// 单例时为NULL
        /// </summary>
        public Type ViewModel { get; set; } = null;

        public int CompareTo(MenuViewModel p)
        {
            int result = 0;
            if (Index == p.Index) result = 0;
            if (Index > p.Index) result = 1;
            if (Index < p.Index) result = -1;
            return result;
        }
    }
}
