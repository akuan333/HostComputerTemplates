﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace UIWindows.Models
{
    public class RunMessageModel
    {
        /// <summary>
        /// 时间
        /// </summary>
        public string RunTime { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }

        public Enums.MessageLevelType Level { get; set; }

        /// <summary>
        /// 修改字体颜色
        /// </summary>
        public Brush MessageForeground { get; set; }
    }
}
