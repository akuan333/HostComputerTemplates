﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace UIWindows.Models
{
    public class ScrollBarModel : INotifyPropertyChanged
    {
        /// <summary>
        /// MES是否在线
        /// </summary>
        [JsonIgnore]
        public bool IsMesOnline { get; set; }

        /// <summary>
        /// 软件是否运行
        /// </summary>
        [JsonIgnore]
        public bool IsRunStatus { get; set; }

        /// <summary>
        /// 是否滚动
        /// </summary>
        [JsonIgnore]
        public bool IsRunning { get; set; } = false;

        [JsonIgnore]
        public string RunningBlockText { get; set; }

        [JsonIgnore]
        public System.Windows.Media.Brush RunBackground { get; set; } = Tools.Helpers.ThemeHelper.RunBrush;
        /// <summary>
        /// 滚动速度
        /// </summary>
        public int Speed { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        private ConcurrentDictionary<long, string> _keyValuePairs = new ConcurrentDictionary<long, string>();

        /// <summary>
        /// 添加滚动项
        /// </summary>
        /// <param name="value">用来标识项，移除时有用</param>
        /// <param name="str">滚动显示字符串</param>
        public void AddKeyValue(long value, string str)
        {
            if (!_keyValuePairs.ContainsKey(value) 
                && _keyValuePairs.Count < 5 
                && _keyValuePairs.TryAdd(value, str))
            {
                ShowStr();
            }
        }

        /// <summary>
        /// 移除滚动项
        /// </summary>
        /// <param name="value">0为全部移除</param>
        public void RemoveKeyValue(long value = 0)
        {
            if (value == 0)
            {
                IsRunning = false;
                RunningBlockText = null;
                _keyValuePairs.Clear();
            }
            if (_keyValuePairs.TryRemove(value, out string str))
            {
                ShowStr();
            }
        }

        private void ShowStr()
        {
            IsRunning = false;
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in _keyValuePairs)
            {
                stringBuilder.Append($"警告：{item}\t");
            }
            RunningBlockText = stringBuilder.ToString();
            IsRunning = true;
        }
    }
}
