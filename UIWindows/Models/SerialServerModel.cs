﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Models
{
    public class SerialServerModel
    {
        /// <summary>
        /// 设备标识
        /// </summary>
        public string Mark { get; set; }
        /// <summary>
        /// 设备MAC
        /// </summary>
        public string MAC { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        public string IPAddress { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; }
    }
}
