﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace UIWindows.Models
{
    public class SystemConfigModel
    {
        /// <summary>
        /// 亮暗主题切换
        /// </summary>
        public bool IsDarkTheme { get; set; }

        /// <summary>
        /// 原色
        /// </summary>
        public Color? PrimaryColor { get; set; }

        /// <summary>
        /// 二次色
        /// </summary>
        public Color? SecondaryColor { get; set; }

        /// <summary>
        /// 窗体名
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Read间隔时间
        /// </summary>
        public int Intervals { get; set; }
    }
}
