﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.XingLucifer;

namespace UIWindows.Services
{
    public abstract class TaskEventBase<T> where T : IDataContext, new()
    {
        public TaskEventBase()
        {

        }

        public virtual Task Handle(T context)
        {
            //业务处理
            return Task.CompletedTask;
        }
    }
}
