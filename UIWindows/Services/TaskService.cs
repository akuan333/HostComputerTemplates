﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using UIWindows.Enums;
using UIWindows.GlobalStatics;
using UIWindows.Domains;
using UIWindows.XingLucifer;
using UIWindows.Tools.Helpers;

namespace UIWindows.Services
{
    public class TaskService
    {
        #region 单例模式
        private readonly static object _obj = new object();
        private static TaskService _taskService;
        public static TaskService Instance
        {
            get
            {
                if (_taskService == null)
                {
                    lock (_obj)
                    {
                        if (_taskService == null) _taskService = new TaskService();
                    }
                }
                return _taskService;
            }
        }
        #endregion

        private Brush _runOne = new SolidColorBrush(Color.FromArgb(0xFF, 0x41, 0x9E, 0x00));
        private Brush _runTwo = new SolidColorBrush(Color.FromArgb(0xFF, 0x93, 0xFF, 0x48));
        private CancellationTokenSource _taskToken;
        private DeviceConfigStatic _deviceConfig;
        private SystemConfigStatic _systemConfig;
        private TaskService()
        {
            _taskToken = new CancellationTokenSource();
            _deviceConfig = ObjectContainer.ResolveSingleton<DeviceConfigStatic>();
            _systemConfig = ObjectContainer.ResolveSingleton<SystemConfigStatic>();
        }

        /// <summary>
        /// 主任务
        /// </summary>
        public IMainTask MainTaskAction { get; set; }

        /// <summary>
        /// 停止
        /// </summary>
        public void Stop() => _taskToken.Cancel();

        /// <summary>
        /// 启动
        /// </summary>
        public async Task<bool> SwitchOn()
        {
            if (_taskToken.Token.IsCancellationRequested) { _taskToken = new CancellationTokenSource(); }
            if (await DeviceLink())
            {
                int count = 0;
                PingDevice(_taskToken);
                _systemConfig.MainMvvm.RunBackground = _runOne;
                _systemConfig.MainMvvm.IsRunStatus = true;
                TaskManager.RemoveAll();
                _ = Task.Run(() =>
                {
                    while (!_taskToken.Token.IsCancellationRequested)
                    {
                        MainTaskAction?.Handle(_taskToken);
                        count++;
                        if (count == 10)
                        {
                            if (_systemConfig.MainMvvm.RunBackground == _runOne)
                            {
                                _systemConfig.MainMvvm.RunBackground = _runTwo;
                            }
                            else
                            {
                                _systemConfig.MainMvvm.RunBackground = _runOne;
                            }
                            count = 0;
                        }
                        Thread.Sleep(_systemConfig.SCM.Intervals);
                    }
                }, _taskToken.Token).ContinueWith(sender =>
                {
                    MainTaskAction?.Handle();
                    _systemConfig.MainMvvm.RunBackground = ThemeHelper.RunBrush;
                    _systemConfig.MainMvvm.IsRunStatus = false;
                });
                return true;
            }
            return false;
        }

        public async Task<bool> DeviceLink()
        {
            return await Task.Run(() => Parallel.ForEach(_deviceConfig.DeviceList, (x, _parallelLoopState) =>
            {
                if (x.Index == 999) { return; }
                ICommunication model = null;
                //if (x.IsSocket)
                //{
                //    if (x.DeviceType == DeviceTypes.注液机) model = new CommS7(x.IPofCom);
                //    else model = new CommTCP(x.IPofCom, x.Port, x.Index);
                //}
                //else
                //{
                //    model = new CommSerialPort(x.IPofCom, x.Port, x.Index);
                //}
                //model.ReconnectDisconnection += Device_DropAlarms;
                //if (model?.Open())
                //{
                //    if (x.Types == DeviceTypes.前称重 || x.Types == DeviceTypes.后称重) model.Read(_mainGlobal.SocketToken);
                //    if (x.Types == DeviceTypes.注液泵)
                //    {
                //        model.Write(new FinsHandshakePackage() { Address = (byte)_parameter.RunParameter.FinsIP });
                //        Thread.Sleep(20);
                //        var shakeHands = new ResponseShakeHands().Handle(model.Read());
                //        if (shakeHands != null) x.DA1 = (byte)(shakeHands as ResponseShakeHands).ServerAddress;
                //        else throw new Exception("通信异常");
                //    }
                //    x.Communication = model;
                //}
                //else
                //{
                //    $"设备[{x.DeviceType}] 序号[{x.Index}] IP[{x.IPCOM}] 端口号[{x.Port}] 连接失败".RunLog(MessageLevelType.错误);
                //    _parallelLoopState.Stop();
                //}
            }).IsCompleted);
        }

        private bool Device_DropAlarms(ICommunication com, int index, string ipcom, int port, DeviceTypes deviceTypes, CancellationToken token)
        {
            Models.DeviceClientModel device = _deviceConfig.DeviceList.FirstOrDefault(t => t.DeviceType == deviceTypes && t.IPCOM == ipcom && t.Index == index && t.Port == port);
            device.IsOnline = false;
            if (token.IsCancellationRequested) 
            { 
                device.IsOnline = null; 
                return false;
            }
            com.Close();
            Thread.Sleep(200);
            if (com.Open())
            {
                $"设备[{deviceTypes}] 序号[{index}] IP[{ipcom}] 端口号[{port}]重连成功！".RunLog(MessageLevelType.提醒);
                device.IsOnline = true;
                return true;
            }
            $"设备[{deviceTypes}] 序号[{index}] IP[{ipcom}] 端口号[{port}]重连失败...".RunLog(MessageLevelType.提醒);
            return false;
        }

        /// <summary>
        /// 启动任务成功后，定时PING设备是否正常通信
        /// </summary>
        /// <param name="tokenSource"></param>
        private void PingDevice(CancellationTokenSource tokenSource)
        {
            Task.Run(() =>
            {
                while (!tokenSource.Token.IsCancellationRequested)
                {
                    foreach (var model in _deviceConfig.DeviceList)
                    {
                        if (!model.IsSocket || model.Communication == null) { continue; }
                        try
                        {
                            var message = ICMPHelper.PING(model.IPCOM);
                            model.PingTime = message.Time;
                            model.ICMPResult = message.Result;
                        }
                        catch (Exception ex)
                        {
                            $"设备[{model.DeviceType}] 序号[{model.Index}] IP[{model.IPCOM}] 端口号[{model.Port}] 异常信息：{ex}".RunLog(MessageLevelType.警告);
                        }
                        Thread.Sleep(50);
                    }
                    Thread.Sleep(5000);
                }
            });
        }
    }
}
