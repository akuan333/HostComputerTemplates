﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace UIWindows.Tools.Converter
{
    [ValueConversion(typeof(bool), typeof(Brush))]
    public class BoolToColosConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null
                ? new SolidColorBrush(Color.FromArgb(0xFF, 0xF3, 0x51, 0x51))
                : System.Convert.ToBoolean(value) ? new SolidColorBrush(Color.FromArgb(0xF9, 0x08, 0xB9, 0x89)) : new SolidColorBrush(Color.FromArgb(0xFF, 0xF3, 0xCE, 0x51));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
