﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace UIWindows.Tools.Converter
{
    [ValueConversion(typeof(bool), typeof(Brush))]
    public class MESBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null
                ? new SolidColorBrush(Color.FromArgb(0xFF, 0xF7, 0xB1, 0x1C)) : System.Convert.ToBoolean(value) ? new SolidColorBrush(Color.FromArgb(0xFF, 0x55, 0xDC, 0x3F)) : new SolidColorBrush(Color.FromArgb(0xFF, 0xF7, 0xB1, 0x1C));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
