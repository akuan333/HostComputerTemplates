﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Text.Json;
using System.IO;
using System.Windows;

namespace UIWindows.Tools.Converter
{
    [ValueConversion(typeof(string), typeof(string))]
    public class StringToTitleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //string title = "0.0.0.0";
            //try
            //{
            //    var model = System.Windows.Application.ResourceAssembly;
            //    title = System.Diagnostics.FileVersionInfo.GetVersionInfo($"{Environment.Version.ToString}\\UIWindows.dll")?.FileVersion;
            //}
            //catch (Exception)
            //{}
            return $"{value}       v {Environment.Version.ToString()} NET 5";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
