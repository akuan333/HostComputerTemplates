﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.Tools.Helpers
{
    public static class ConversionHelper
    {
        public static T UpdateValue<T>(this T t, T obj)
        {
            var objProper = obj.GetType().GetProperties();
            foreach (var item in t.GetType().GetProperties())
            {
                objProper.Where(x => x.Name == item.Name).FirstOrDefault().SetValue(obj, item.GetValue(t, null));
            }
            return obj;
        }

        /// <summary>
        /// 检测当前时间是否是年份交换或季节交换节点
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool ContrastTime(this DateTime dateTime) => (dateTime.Month == 1 || dateTime.Month == 4 || dateTime.Month == 7 || dateTime.Month == 10) && dateTime.Day == 1;

        /// <summary>
        /// 计算季度===三个月为一季
        /// </summary>
        /// <param name="count">当前时间</param>
        /// <returns>1 = 1-3月；2 = 4-6月；3 = 7-9月；4 = 10-12月</returns>
        public static int GetQuarterly(this DateTime dateTime)
        {
            return dateTime.Month switch
            {
                int p when p is >= 1 and < 4 => 1,
                int p when p is >= 4 and < 7 => 2,
                int p when p is >= 7 and < 10 => 3,
                int p when p is >= 10 and <= 12 => 4,
                _ => 1,
            };
        }
    }
}
