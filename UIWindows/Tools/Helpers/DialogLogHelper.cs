﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using UIWindows.Enums;
using UIWindows.ViewModels.DialogModels;
using UIWindows.Views.Dialogs;
using log4net;
using UIWindows.GlobalStatics;

namespace UIWindows.Tools.Helpers
{
    public static class DialogLogHelper
    {
        private static Brush _error = null;
        private static Brush _warn;
        private static Brush _info;
        private static readonly object _obj;
        private static HomeStatic _homeStatic;

        public static void Init()
        {
            if (_error == null)
            {
                _error = new SolidColorBrush(Color.FromArgb(0xFF, 0xF3, 0x51, 0x51));
                _warn = new SolidColorBrush(Color.FromArgb(0xFF, 0xF3, 0xCE, 0x51));
                _info = new SolidColorBrush(Color.FromArgb(0xF9, 0x08, 0xB9, 0x89));
                _homeStatic = Domains.ObjectContainer.ResolveSingleton<HomeStatic>();
            }
        }

        private static Func<string, MessageLevelType, Task<object>> _messageDialogs = null;
        private static ILog _mes = LogManager.GetLogger("mes");
        private static ILog _run = LogManager.GetLogger("message");
        private static ILog _dialogs = LogManager.GetLogger("dialog");
        /// <summary>
        /// 消息弹窗
        /// </summary>
        /// <param name="str">消息</param>
        /// <param name="levelType">消息等级枚举</param>
        public static Task<object> MessageDialogs(this string str, MessageLevelType levelType, Visibility visibility = Visibility.Collapsed)
        {
            if (_messageDialogs == null)
            {
                _messageDialogs = new Func<string, MessageLevelType, Task<object>>(async (str, levelType) =>
                {
                    if (DialogHost.IsDialogOpen("RootDialog")) DialogHost.Close("RootDialog");
                    string title = levelType.ToString();
                    Brush brush = null;
                    PackIconKind pack = PackIconKind.AllInclusive;
                    title = $"{title[0]}  {title[1]}";
                    switch (levelType)
                    {
                        case MessageLevelType.错误:
                            brush = _error;
                            pack = PackIconKind.AlertCircleOutline;
                            _dialogs.Error(str);
                            break;
                        case MessageLevelType.信息:
                            brush = _info;
                            pack = PackIconKind.BookInformationVariant;
                            _dialogs.Info(str);
                            break;
                        case MessageLevelType.警告:
                            brush = _warn;
                            pack = PackIconKind.AlertOutline;
                            _dialogs.Warn(str);
                            break;
                        case MessageLevelType.注意:
                            brush = _warn;
                            pack = PackIconKind.FolderAlertOutline;
                            _dialogs.Warn(str);
                            break;
                        case MessageLevelType.调试:
                            brush = _info;
                            pack = PackIconKind.AndroidDebugBridge;
                            _dialogs.Debug(str);
                            break;
                        case MessageLevelType.紧急:
                            brush = _error;
                            pack = PackIconKind.AlarmLightOutline;
                            _dialogs.Warn(str);
                            break;
                        case MessageLevelType.严重:
                            brush = _error;
                            pack = PackIconKind.Alarm;
                            _dialogs.Error(str);
                            break;
                        case MessageLevelType.提醒:
                            brush = _info;
                            pack = PackIconKind.Reminder;
                            _dialogs.Warn(str);
                            break;
                    }
                    return await DialogHost.Show(new MessageDialogs()
                    {
                        DataContext = new MessageDialogsViewModel(title, str, pack, brush, visibility)
                    }, "RootDialog");
                });
            }
            return ThreadHelper.IsUIThread(() => _messageDialogs(str, levelType), () => ThreadHelper.CrossThreadAsync(sender => _messageDialogs(str, levelType), _obj));
        }

        /// <summary>
        /// 运行日志
        /// </summary>
        /// <param name="str"></param>
        /// <param name="levelType"></param>
        public static void RunLog(this string str, MessageLevelType levelType)
        {
            switch (levelType)
            {
                case MessageLevelType.错误:
                    _run.Error(str);
                    LogAdd(str, _error, levelType);
                    break;
                case MessageLevelType.信息:
                    _run.Info(str);
                    LogAdd(str, _info, levelType);
                    break;
                case MessageLevelType.警告:
                    _run.Warn(str);
                    LogAdd(str, _warn, levelType);
                    break;
                case MessageLevelType.注意:
                    _run.Warn(str);
                    LogAdd(str, _warn, levelType);
                    break;
                case MessageLevelType.调试:
                    _run.Debug(str);
                    LogAdd(str, _info, levelType);
                    break;
                case MessageLevelType.紧急:
                    _run.Warn(str);
                    LogAdd(str, _error, levelType);
                    break;
                case MessageLevelType.严重:
                    _run.Error(str);
                    LogAdd(str, _error, levelType);
                    break;
                case MessageLevelType.提醒:
                    _run.Warn(str);
                    LogAdd(str, _warn, levelType);
                    break;
            }
            if (_homeStatic.LogList.Count > 400)
            {
                _homeStatic.LogList.RemoveAt(399);
            }
        }
        private static void LogAdd(string str, Brush brush, MessageLevelType levelType)
        {
            _homeStatic.LogList.Insert(0, new Models.RunMessageModel()
            {
                Level = levelType,
                Message = str,
                MessageForeground = brush,
                RunTime = DateTime.Now.ToString("T"),
            });
        }
    }
}
