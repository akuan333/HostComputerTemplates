﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIWindows.Enums;
using UIWindows.XingLucifer;

namespace UIWindows.Tools.Helpers
{
    public static class DisconnectionHelper
    {
        /// <summary>
        /// 重连执行前委托
        /// </summary>
        public static Action<int, string, int, DeviceTypes> BeforeExecutionDisconnection { get; set; }

        /// <summary>
        /// 重连执行后委托
        /// </summary>
        public static Action<int, string, int, DeviceTypes, bool, string> AfterExecutionDisconnection { get; set; }

        public static bool CommunicationDisconnection(ICommunication communication, int index, string ipcom, int port, DeviceTypes types)
        {
            bool value = true;
            string str = string.Empty;
            BeforeExecutionDisconnection?.Invoke(index, ipcom, port, types);
            try
            {
                communication.Close();
                Thread.Sleep(200);
                communication.Open();
            }
            catch (Exception ex)
            {
                value = false;
                str = ex.ToString();
            }
            AfterExecutionDisconnection?.Invoke(index, ipcom, port, types, value, str);
            Thread.Sleep(100);
            return value;
        }
    }
}
