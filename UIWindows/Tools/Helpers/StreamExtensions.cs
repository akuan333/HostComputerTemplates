﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace UIWindows.Tools.Helpers
{
    /// <summary>
    /// 流读取帮助类
    /// </summary>
    public static class StreamExtensions
    {
        public static async Task<int> ReadExactAsync(this Stream stream, byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            int read = 0;
            int received;
            for (; ; )
            { 
                received = await stream.ReadAsync(buffer.AsMemory(offset + read, count - read), cancellationToken).ConfigureAwait(false);
                read += received;
                if (read >= count && received <= 0) return read;
            }
        }
    }
}
