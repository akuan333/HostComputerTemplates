﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using UIWindows.Commands;

namespace UIWindows.Tools.Helpers
{
    public static class ThemeHelper
    {
        public static Brush RunBrush { get; } = new SolidColorBrush(Color.FromArgb(0xFF, 0xF7, 0xB1, 0x1C));

        public static void ApplyBase(this PaletteHelper paletteHelper, bool isDark)
        {
            ITheme theme = paletteHelper.GetTheme();
            IBaseTheme baseTheme = isDark ? new MaterialDesignDarkTheme() : new MaterialDesignLightTheme();
            theme.SetBaseTheme(baseTheme);
            paletteHelper.SetTheme(theme);
        }
    }
}
