﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIWindows.Attributes;
using UIWindows.Enums;
using UIWindows.Models;
using UIWindows.XingLucifer;

namespace UIWindows.Tools
{
    public static class StatusPublisher
    {
        private static List<ManagerModel> _list;

        private static Task _task = Task.FromResult(new object());


        static StatusPublisher()
        {
            _list = new List<ManagerModel>();
        }

        /// <summary>
        /// 发布任务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="name">任务名</param>
        /// <param name="type">任务类型</param>
        /// <param name="obj">参数</param>
        /// <returns></returns>
        public static bool Publish<T>(this T value, string name, TriggerType type, object obj) where T : struct
        {
            var tcs = new TaskCompletionSource();
            _ = Interlocked.Exchange(ref _task, tcs.Task);
            var model = _list.Where(x => x.Name == name)?.FirstOrDefault();
            try
            {
                if (model == null) return false;
                switch (typeof(T).Name)
                {
                    case "Boolean": break;
                    case "Int32": break;
                    case "UInt32": break;
                    case "Int16": break;
                    case "UInt16": break;
                    case "Single": break;
                    case "Double": break;
                    case "Byte": break;
                    case "Int64": break;
                    case "UInt64": break;
                }
                model.TaskEvent.Invoke(obj);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally { 
                tcs.SetResult();
            }
        }

        /// <summary>
        /// 订阅任务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">初始值</param>
        /// <param name="name">任务名</param>
        /// <param name="type">任务类型</param>
        /// <param name="action">任务委托</param>
        public static void Subscription<T>(T value, string name, TriggerType type, Action<object> action) where T : struct
            => _list.Add(new ManagerModel()
            {
                Types = type,
                Name = name,
                Value = value,
                TaskEvent = action
            });
    }
}
