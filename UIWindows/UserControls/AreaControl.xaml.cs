﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UIWindows.UserControls
{
    /// <summary>
    /// AreaControl.xaml 的交互逻辑
    /// </summary>
    public partial class AreaControl : UserControl
    {
        public AreaControl()
        {
            InitializeComponent();
        }

        public string Title
        {
            set => SetValue(MyTitleProperty, value);
            get => GetValue(MyTitleProperty) as string;
        }
        public static readonly DependencyProperty MyTitleProperty =
    DependencyProperty.Register("Title", typeof(string), typeof(AreaControl), new FrameworkPropertyMetadata("区域"));

        public FrameworkElement ViewContent
        {
            set => SetValue(MyContentProperty, value);
            get => GetValue(MyContentProperty) as FrameworkElement;
        }
        public static readonly DependencyProperty MyContentProperty =
            DependencyProperty.Register("ViewContent", typeof(FrameworkElement), typeof(AreaControl),
                new FrameworkPropertyMetadata(default(FrameworkElement), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
    }
}
