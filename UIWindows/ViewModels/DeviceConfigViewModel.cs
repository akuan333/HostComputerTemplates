﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.XingLucifer;
using MaterialDesignThemes.Wpf;
using System.Collections.ObjectModel;
using UIWindows.Models;
using UIWindows.Domains;
using UIWindows.GlobalStatics;
using UIWindows.Commands;
using System.Net.Sockets;
using UIWindows.Enums;
using System.Net;
using System.Windows.Controls;
using UIWindows.Views.Dialogs;
using UIWindows.Domains.BroadcastDeviceObject;
using UIWindows.Tools.Helpers;

namespace UIWindows.ViewModels
{
    public class DeviceConfigViewModel : IViewStatus
    {
        public ObservableCollection<SerialServerModel> DeviceList { get; set; }
        /// <summary>
        /// 是否是多设备
        /// </summary> 
        public bool IsDevices { get; set; }
        public bool IsNet { get; set; }
        public DeviceTypes Types { get; set; }
        private readonly UdpClient udpClient;
        public string StartAddress { get; set; }
        public int StartProt { get; set; }
        public string EndAddress { get; set; }
        public int EndPort { get; set; }
        public string SerialServerName { get; set; }

        public DeviceConfigStatic MyDevice { get; set; }

        public DeviceConfigViewModel(DeviceConfigStatic deviceConfig)
        {
            MyDevice = deviceConfig;
            DeviceList = new ObservableCollection<SerialServerModel>();
            udpClient = new UdpClient(new IPEndPoint(IPAddress.Any, 22345));
            udpClient.Client.ReceiveTimeout = 3000;
            IsNet = true;
        }

        public DelegateCommand AfterWeighingServer => new DelegateCommand(() =>
        {
            if (IsDevices)
            {
                MyDevice.AddDevices(StartAddress, StartProt.ToString(), EndAddress, EndPort.ToString(), IsNet, Types);
            }
            else
            {
                MyDevice.Add(StartAddress, StartProt, StartProt, IsNet, Types);
            }
        });

        public DelegateCommand<SerialServerModel> MouseDoubleClick => new DelegateCommand<SerialServerModel>(sender => System.Diagnostics.Process.Start("explorer.exe", $"http://{sender?.IPAddress}"));

        public IEnumerable<DeviceTypes> Foods
        {
            get
            {
                foreach (DeviceTypes c in (DeviceTypes[])Enum.GetValues(typeof(DeviceTypes)))
                {
                    yield return c;
                }
            }
        }

        public DelegateCommand SaveInit => new DelegateCommand(() => 
        {
            MyDevice.Save();
            _ = "设备配置，已保存成功！！！".MessageDialogs(MessageLevelType.信息).ConfigureAwait(false);
        });

        /// <summary>
        /// 搜索设备
        /// </summary>
        public DelegateCommand<Button> SearchDevice => new DelegateCommand<Button>(async sender =>
        {
            DeviceList.Clear();
            _ = DialogHost.Show(new LoadingDialogs(), "RootDialog").ConfigureAwait(false);
            var list = await Task.Run(() =>
            {
                ISerialServer serialServer = GetSerialServer();
                List<SerialServerModel> list = new List<SerialServerModel>();
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, 000);
                byte[] buffer = serialServer.GetStream();
                foreach (var item in Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork))
                {
                    string[] strIP = item.ToString().Split('.');
                    udpClient.Send(buffer, buffer.Length, new IPEndPoint(IPAddress.Parse($"{strIP[0]}.{strIP[1]}.{strIP[2]}.255"), serialServer.Port));
                }
                while (true)
                {
                    try
                    {
                        byte[] bytes = udpClient.Receive(ref endpoint);
                        var model = serialServer.Parsing(bytes, endpoint);
                        if (!list.Any(x => x.IPAddress == model.IPAddress)) list.Add(model);
                    }
                    catch (SocketException)
                    {
                        break;
                    }
                }
                return list;
            });
            foreach (var item in list)
            {
                DeviceList.Add(new SerialServerModel()
                {
                    Mark = item.Mark,
                    MAC = item.MAC,
                    IPAddress = item.IPAddress,
                    Number = item.Number
                });
            }
            DialogHost.CloseDialogCommand.Execute(new object(), sender);
        });

        private ISerialServer GetSerialServer()
        {
            return SerialServerName switch
            {
                "IP_Search(1.2)" => new IP_Search_1_2(),
                "HLK_Discover" => new HLK_Discover(),
                _ => null,
            };
        }

        public void Load()
        {

        }

        public void UnLoad()
        {
            udpClient.Close();
        }
    }
}
