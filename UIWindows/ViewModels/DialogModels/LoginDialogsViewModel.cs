﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using UIWindows.Commands;

namespace UIWindows.ViewModels.DialogModels
{
    public class LoginDialogsViewModel : Domains.ViewModelBase
    {
        private string _name;
        /// <summary>
        /// 账号
        /// </summary>
        public string Name
        {
            get { return _name; }
            set => SetProperty(ref _name, value);
        }
        private string _loginMessage;
        /// <summary>
        /// 登录消息
        /// </summary>
        public string LoginMessage
        {
            get { return _loginMessage; }
            set => SetProperty(ref _loginMessage, value);
        }

        private Brush _textBrush;
        /// <summary>
        /// 文本颜色
        /// </summary>
        public Brush TextBrush
        {
            get { return _textBrush; }
            set => SetProperty(ref _textBrush, value);
        }

        private GlobalStatics.UserConfigStatic _global;

        public LoginDialogsViewModel()
        {
            _global = Domains.ObjectContainer.ResolveSingleton<GlobalStatics.UserConfigStatic>();
        }

        public DelegateCommand<PasswordBox> Login => new DelegateCommand<PasswordBox>(password =>
        {
            TextBrush = Brushes.Black;
            LoginMessage = $"登陆中，请等待...";
            if ( _global.Login(Name, password.Password))
            {
                if (DialogHost.IsDialogOpen("RootDialog")) 
                {
                    DialogHost.Close("RootDialog");
                }
            }
            else 
            {
                LoginMessage = $"登录失败，账号或密码错误";
                TextBrush = Brushes.Red;
                password.Password = null;
            }
        });
    }
}
