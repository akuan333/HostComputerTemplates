﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace UIWindows.ViewModels.DialogModels
{
    public class MessageDialogsViewModel
    {
        /// <summary>
        /// 图标
        /// </summary>
        public PackIconKind KindName { get; set; }
        public string Title { get; set; }
        public string MessageTxt { get; set; }
        public Brush MessageForeground { get; set; }
        public Visibility MyVisibility { get; set; }


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="txt">消息内容</param>
        /// <param name="kind">图标</param>
        /// <param name="brush">图标颜色</param>
        public MessageDialogsViewModel(string title, string txt, PackIconKind kind, Brush brush, Visibility visibility)
        {
            MessageTxt = txt;
            KindName = kind;
            Title = title;
            MessageForeground = brush;
            MyVisibility = visibility;
        }
    }
}
