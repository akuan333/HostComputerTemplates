﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.XingLucifer;
using UIWindows.Domains;
using UIWindows.GlobalStatics;
using UIWindows.Commands;
using System.Threading;
using UIWindows.Tools.Helpers;

namespace UIWindows.ViewModels
{
    public class HomePageViewModel : ViewModelBase, IViewStatus
    {
        public HomeStatic HStatic { get; set; }
        public SystemConfigStatic SystemConfig { get; set; }
        public HomePageViewModel(HomeStatic homeStatic, SystemConfigStatic systemConfig)
        {
            HStatic = homeStatic;
            SystemConfig = systemConfig;
        }

        public DelegateCommand DialogsCommand => new DelegateCommand(() => "这是一个弹窗".MessageDialogs(Enums.MessageLevelType.提醒).ConfigureAwait(false));

        public DelegateCommand ReturnDialogsCommand => new DelegateCommand(async () =>
        {
            var bools = await "具有返回值的异步弹窗".MessageDialogs(Enums.MessageLevelType.注意, System.Windows.Visibility.Visible);
            $"异步弹窗返回的值：{bools}".RunLog(Enums.MessageLevelType.信息);
        });

        public DelegateCommand ThreadLogCommand => new DelegateCommand(() =>
        {
            byte[] bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            for (int i = 0; i < 5; i++)
            {
                Task.Run(() => {
                    for (int i = 0; i < 500; i++)
                    {
                        Parallel.ForEach(bytes, x =>
                        {
                            if (x % 3 == 0)
                            {
                                "x % 3".RunLog(Enums.MessageLevelType.严重);
                            }
                            if (x % 2 == 0)
                            {
                                "x % 2".RunLog(Enums.MessageLevelType.信息);
                            }
                            if (x % 4 == 0)
                            {
                                "x % 4".RunLog(Enums.MessageLevelType.警告);
                            }
                        });
                    }
                });
            }
        });

        public DelegateCommand ScrollBarCommand => new DelegateCommand(() => SystemConfig.MainMvvm.AddKeyValue(12560, "这是添加的滚动，最多显示五条"));

        public DelegateCommand ClearScrollBarCommand => new DelegateCommand(() => SystemConfig.MainMvvm.RemoveKeyValue());

        public void Load()
        {
            //触发界面
        }

        public void UnLoad()
        {
            //丢失界面
        }
    }
}
