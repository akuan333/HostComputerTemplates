﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.GlobalStatics;
using UIWindows.Tools.Helpers;

namespace UIWindows.ViewModels
{
    public class MainWindowViewModel
    {
        public SystemConfigStatic SCStatic { get; }
        public MainWindowViewModel()
        {
            SCStatic = Domains.ObjectContainer.ResolveSingleton<SystemConfigStatic>();
            var paletteHelper = new PaletteHelper();
            ITheme theme = paletteHelper.GetTheme();
            paletteHelper.ChangePrimaryColor(SCStatic.SCM.PrimaryColor ?? theme.PrimaryMid.Color);
            paletteHelper.ApplyBase(SCStatic.SCM.IsDarkTheme);
        }
    }
}
