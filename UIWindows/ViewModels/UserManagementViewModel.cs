﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Commands;
using UIWindows.Enums;
using UIWindows.GlobalStatics;
using UIWindows.Models;
using UIWindows.Tools.Helpers;

namespace UIWindows.ViewModels
{
    public class UserManagementViewModel
    {
        public UserConfigStatic UserConfig { get; set; }
        public UserManagementViewModel(UserConfigStatic userConfig)
        {
            UserConfig = userConfig;
        }

        public DelegateCommand AddUpdate => new DelegateCommand(() => UserConfig.Save());

        public DelegateCommand DeleteData => new DelegateCommand(() => UserConfig.Delect());

        public IEnumerable<AuthorityType> Foods
        {
            get
            {
                foreach (AuthorityType c in (AuthorityType[])Enum.GetValues(typeof(AuthorityType)))
                {
                    if ((c != AuthorityType.管理员)
                        && (c != AuthorityType.无权限)
                        && (c != AuthorityType.ALL)
                        && !c.ToString().Contains("参照"))
                    {
                        yield return c;
                    }
                }
            }
        }

        /// <summary>
        /// 双击事件
        /// </summary>
        public DelegateCommand<UserModel> MouseDoubleClick => new DelegateCommand<UserModel>(sender => sender?.UpdateValue(UserConfig.User));
    }
}
