﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UIWindows.Attributes;

namespace UIWindows.Views
{
    /// <summary>
    /// DeviceConfig.xaml 的交互逻辑
    /// </summary>
    [ViewFeature("设备管理", 98, true, Enums.AuthorityType.管理员_参照, MaterialDesignThemes.Wpf.PackIconKind.Devices)]
    public partial class DeviceConfig : UserControl
    {
        public DeviceConfig()
        {
            InitializeComponent();
        }
    }
}
