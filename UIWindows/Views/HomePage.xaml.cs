﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UIWindows.Attributes;

namespace UIWindows.Views
{
    /// <summary>
    /// HomePage.xaml 的交互逻辑
    /// </summary>
    [ViewFeature("主 页", 1, true, Enums.AuthorityType.ALL, MaterialDesignThemes.Wpf.PackIconKind.Home)]
    public partial class HomePage : UserControl
    {
        public HomePage()
        {
            InitializeComponent();
        }
    }
}
