﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UIWindows.Views
{
    /// <summary>
    /// ProductionDataLog.xaml 的交互逻辑
    /// </summary>
    [Attributes.ViewFeature("生产数据", 2, true, Enums.AuthorityType.ALL, MaterialDesignThemes.Wpf.PackIconKind.DatabaseSettingsOutline)]
    public partial class ProductionDataLog : UserControl
    {
        public ProductionDataLog()
        {
            InitializeComponent();
        }
    }
}
