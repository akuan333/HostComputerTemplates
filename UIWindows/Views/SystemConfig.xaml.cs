﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UIWindows.Views
{
    /// <summary>
    /// SystemConfig.xaml 的交互逻辑
    /// </summary>
    [Attributes.ViewFeature("系统配置", 99, true, Enums.AuthorityType.工程师_参照 | Enums.AuthorityType.主管_参照, MaterialDesignThemes.Wpf.PackIconKind.FileCogOutline)]
    public partial class SystemConfig : UserControl
    {
        public SystemConfig()
        {
            InitializeComponent();
        }
    }
}
