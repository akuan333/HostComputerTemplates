﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UIWindows.Enums;

namespace UIWindows.XingLucifer
{
    public interface ICommunication
    {
        /// <summary>
        /// 是否在线
        /// </summary>
        bool IsOnline { get; }
        int Index { get; }
        int Port { get; }
        string IPCOM { get; }
        int Timeout { get; set; }
        DeviceTypes Types { get; }
        List<byte> CacheByte { get; }
        Func<ICommunication, int, string, int, DeviceTypes, CancellationToken, bool> ReconnectDisconnection { get; set; }

        bool Write(byte[] bytes, int length);
        bool WriteSingle<TValue>(TValue value, int address, int offset, int add_type, int count = 3) where TValue : struct;
        bool Write<TArray>(TArray value, int address, int offset, int length, int add_type, int count = 3) where TArray : struct;

        TValue? ReadSingle<TValue>(int address, int offset, int add_type, int count = 3) where TValue : struct;
        TArray? Read<TArray>(int address, int offset, int length, int add_type, int count = 3) where TArray : struct;
        TClass? ReadClass<TClass>(int address, int offset, int add_type, int count = 3) where TClass : class;
        TStruct? ReadStruct<TStruct>(int address, int offset, int add_type, int count = 3) where TStruct : struct;

        Task Read();
        byte[] Read(byte[] bytes, int length, CancellationTokenSource tokenSource);

        bool Open();
        void Close();
    }
}
