﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Enums;

namespace UIWindows.XingLucifer
{
    public interface IDataContext
    {
        TaskTypes Types { get; }
        int AddressSignal { get; set; }
        int AddressComplete { get; set; }
    }
}
