﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UIWindows.XingLucifer
{
    public interface IMainTask
    {
        public void Handle(CancellationTokenSource tokenSource);
        public void Handle();
    }
}
