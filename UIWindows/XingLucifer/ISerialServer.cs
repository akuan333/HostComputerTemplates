﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UIWindows.Models;

namespace UIWindows.XingLucifer
{
    public interface ISerialServer
    {
        public int Port { get; }
        byte[] GetStream();
        SerialServerModel Parsing(byte[] bytes, IPEndPoint endpoint);
    }
}
