﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIWindows.XingLucifer
{
    public interface IViewStatus
    {
        /// <summary>
        /// 加载前触发
        /// </summary>
        void Load();
        /// <summary>
        /// 卸载前触发，注意不要耗时操作，耗时操作的请使用线程池。
        /// </summary>
        void UnLoad();
    }
}
